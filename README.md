# SatNOGS COMMS Software MCU
An open source CubeSat format communication sub-system.
![SatNOGS-COMMS](docs/assets/pcb-top.png)

## Development Guide
SatNOGS COMMS Software MCU in independent of any kind of development tool. 
You can use the development environment of your choice. 

However we recommend the usage of the STM32CubeIDE for the
development, debugging and experimentation.
This IDE has been optimized for the STM32 MCUs and provide helpful 
graphical debugging tools.
More information regarding the import process of the project into this tool 
can be found at [Import into the STM32CubeIDE](#Import into STM32CubeIDE) section.

### Requirements
* CMake (>= 3.16)
* GNU Make
* cross-arm-none-eabi-gcc (>= 5.0)
* clang-format (>= 12.0)

#### Optional
* STM32 CubeMX or STM32CubeIDE (for modifying peripherals and FreeRTOS parameters)
* openOCD (>= 0.11) (for testing)

### Dependencies
The SatNOGS COMMS Software MCU codebase depends on 
[libsatnogs-comms](https://gitlab.com/librespacefoundation/satnogs-comms/libsatnogs-comms) 
 which is shipped as git submodule within the project.

### Coding Style
For the C and C++ code, we use **LLVM** style.
Use `clang-format` and the options file `.clang-format` to
adapt to the styling.

At the root directory of the project there is the `clang-format` options
file `.clang-format` containing the proper configuration.
Developers can import this configuration to their favorite editor.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you import on your editor the coding style rules.

### Import into STM32CubeIDE
- Download STM32CubeIDE
- Open the `satnogs-comms-software-mcu` directory and delete any possible instances of project configuration files (e.g `.project` or `.cproject` and such)
- Launch STM32CubeIDE and go to File -> Import -> Projects from Folder or Archive and find the source directory of satnogs-comms-software-mcu
- Open the `satnogs-comms-software-mcu.ioc` file. This will launch the Device Configuration Tool perspective
- Go to Project -> Generate Code. This will generate the necessary configuration files

Now some final steps are needed:

- Go to Project Properties. Expand the `C/C++ General` Tab. 
- Go to `Paths and Symbols` and add the following directories from workspace:
  * `libsatnogs-comms/include`
  * `libsatnogs-comms/etl/icnlude`
  * `libsatnogs-comms/drivers/rffcx07x-driver/include`
  * `libsatnogs-comms/drivers/at86rf215-driver/include`

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).
You can also chat with the SatNOGS-COMMS development team at
https://riot.im/app/#/room/#satnogs-comms:matrix.org

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png) 
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).
