/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void
Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Mixer_RST_Pin            GPIO_PIN_3
#define Mixer_RST_GPIO_Port      GPIOE
#define MMC_RST_Pin              GPIO_PIN_13
#define MMC_RST_GPIO_Port        GPIOC
#define TCXO_EN_Pin              GPIO_PIN_1
#define TCXO_EN_GPIO_Port        GPIOH
#define EN_Mixer_Pin             GPIO_PIN_0
#define EN_Mixer_GPIO_Port       GPIOC
#define RF_IRQ_Pin               GPIO_PIN_1
#define RF_IRQ_GPIO_Port         GPIOC
#define RF_IRQ_EXTI_IRQn         EXTI1_IRQn
#define FLT_SEL_S_Pin            GPIO_PIN_2
#define FLT_SEL_S_GPIO_Port      GPIOC
#define EN_LNA2_S_Pin            GPIO_PIN_3
#define EN_LNA2_S_GPIO_Port      GPIOC
#define EN_AGC_S_Pin             GPIO_PIN_0
#define EN_AGC_S_GPIO_Port       GPIOA
#define EN_AGC_UHF_Pin           GPIO_PIN_1
#define EN_AGC_UHF_GPIO_Port     GPIOA
#define FLAGB_RX_S_Pin           GPIO_PIN_2
#define FLAGB_RX_S_GPIO_Port     GPIOA
#define EN_RX_S_Pin              GPIO_PIN_3
#define EN_RX_S_GPIO_Port        GPIOA
#define VSET_S_Pin               GPIO_PIN_4
#define VSET_S_GPIO_Port         GPIOA
#define VSET_UHF_Pin             GPIO_PIN_5
#define VSET_UHF_GPIO_Port       GPIOA
#define AGC_TEMP_S_Pin           GPIO_PIN_6
#define AGC_TEMP_S_GPIO_Port     GPIOA
#define GAIN_SET_S_Pin           GPIO_PIN_7
#define GAIN_SET_S_GPIO_Port     GPIOA
#define GAIN_SET_UHF_Pin         GPIO_PIN_4
#define GAIN_SET_UHF_GPIO_Port   GPIOC
#define AGC_TEMP_UHF_Pin         GPIO_PIN_5
#define AGC_TEMP_UHF_GPIO_Port   GPIOC
#define FLT_SEL_UHF_Pin          GPIO_PIN_0
#define FLT_SEL_UHF_GPIO_Port    GPIOB
#define EN_LNA_UHF_Pin           GPIO_PIN_1
#define EN_LNA_UHF_GPIO_Port     GPIOB
#define PA_BYPASS_UHF_Pin        GPIO_PIN_2
#define PA_BYPASS_UHF_GPIO_Port  GPIOB
#define RF_RST_Pin               GPIO_PIN_7
#define RF_RST_GPIO_Port         GPIOE
#define ALERT_T_PA_U_Pin         GPIO_PIN_8
#define ALERT_T_PA_U_GPIO_Port   GPIOE
#define FLAGB_TX_UHF_Pin         GPIO_PIN_9
#define FLAGB_TX_UHF_GPIO_Port   GPIOE
#define FLAGB_RX_UHF_Pin         GPIO_PIN_10
#define FLAGB_RX_UHF_GPIO_Port   GPIOE
#define EN_RX_UHF_Pin            GPIO_PIN_11
#define EN_RX_UHF_GPIO_Port      GPIOE
#define ALERT_T_PCB_Pin          GPIO_PIN_12
#define ALERT_T_PCB_GPIO_Port    GPIOE
#define P3_3V_ALERT_C_Pin        GPIO_PIN_13
#define P3_3V_ALERT_C_GPIO_Port  GPIOE
#define P5V_M_WARNING_Pin        GPIO_PIN_14
#define P5V_M_WARNING_GPIO_Port  GPIOE
#define P5V_M_CRITICAL_Pin       GPIO_PIN_15
#define P5V_M_CRITICAL_GPIO_Port GPIOE
#define BUS_SCL_Pin              GPIO_PIN_10
#define BUS_SCL_GPIO_Port        GPIOB
#define BUS_SDA_Pin              GPIO_PIN_11
#define BUS_SDA_GPIO_Port        GPIOB
#define PMIC_EN_OUT_Pin          GPIO_PIN_8
#define PMIC_EN_OUT_GPIO_Port    GPIOD
#define PMIC_SLEEP_Pin           GPIO_PIN_9
#define PMIC_SLEEP_GPIO_Port     GPIOD
#define ANT_DET_1_Pin            GPIO_PIN_10
#define ANT_DET_1_GPIO_Port      GPIOD
#define ANT_DEP_1_Pin            GPIO_PIN_11
#define ANT_DEP_1_GPIO_Port      GPIOD
#define ANT_DET_0_Pin            GPIO_PIN_12
#define ANT_DET_0_GPIO_Port      GPIOD
#define ANT_DEP_0_Pin            GPIO_PIN_13
#define ANT_DEP_0_GPIO_Port      GPIOD
#define ALERT_T_PA_S_Pin         GPIO_PIN_14
#define ALERT_T_PA_S_GPIO_Port   GPIOD
#define FLAGB_TX_S_Pin           GPIO_PIN_15
#define FLAGB_TX_S_GPIO_Port     GPIOD
#define PA_BYPASS_S_Pin          GPIO_PIN_6
#define PA_BYPASS_S_GPIO_Port    GPIOC
#define P5V_PG_Pin               GPIO_PIN_7
#define P5V_PG_GPIO_Port         GPIOC
#define MIXER_SDA_Pin            GPIO_PIN_9
#define MIXER_SDA_GPIO_Port      GPIOC
#define MIXER_SCL_Pin            GPIO_PIN_8
#define MIXER_SCL_GPIO_Port      GPIOA
#define P3V3_RF_PG_Pin           GPIO_PIN_9
#define P3V3_RF_PG_GPIO_Port     GPIOA
#define P3V3_RF_EN_Pin           GPIO_PIN_10
#define P3V3_RF_EN_GPIO_Port     GPIOA
#define CAN1_EN_Pin              GPIO_PIN_3
#define CAN1_EN_GPIO_Port        GPIOD
#define CAN1_STB_Pin             GPIO_PIN_4
#define CAN1_STB_GPIO_Port       GPIOD
#define CAN2_EN_Pin              GPIO_PIN_5
#define CAN2_EN_GPIO_Port        GPIOD
#define CAN2_STB_Pin             GPIO_PIN_7
#define CAN2_STB_GPIO_Port       GPIOD
#define EN_LNA1_S_Pin            GPIO_PIN_9
#define EN_LNA1_S_GPIO_Port      GPIOB
#define MIXER_ENX_Pin            GPIO_PIN_0
#define MIXER_ENX_GPIO_Port      GPIOE
#define FLAGB_MIXER_Pin          GPIO_PIN_1
#define FLAGB_MIXER_GPIO_Port    GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
